export HOMF=/home/u
export PROJ=$HOMF/Documents/HelloAndroid
#export PLAT=$HOMF/tools/platforms
export AAPT=$HOMF/Android/Sdk/build-tools/27.0.3/aapt
export DX=$HOMF/Android/Sdk/build-tools/27.0.3/dx

export APKSIGNER=$HOMF/Android/Sdk/build-tools/27.0.3/apksigner

export ANDROIDJAR=$HOMF/Android/Sdk/platforms/android-19/android.jar

export NDK=$HOMF/tools/android-ndk-r17
PATH=$PATH:$NDK

export APP_PLATFORM=android-19

rm $PROJ/bin/classes.dex 
echo "delete binary file"
rm $PROJ/bin/hello.unaligned.apk
rm obj/com/example/helloandroid/*
rm libhello.so
rm -rf lib
# cd Android/Sdk/build-tools/27.0.3/ 
echo "creating R.java file"
$AAPT package -f -m -J $PROJ/src -M $PROJ/AndroidManifest.xml -S $PROJ/res -I $ANDROIDJAR



#/home/u/Android/Sdk/build-tools/27.0.3
javac -d obj -classpath src -bootclasspath $ANDROIDJAR -source 1.7 -target 1.7 src/com/example/helloandroid/*.java
echo "building *.class"


echo "building dex file"
#/home/u/Android/Sdk/build-tools/27.0.3/
$DX --dex --output=$PROJ/bin/classes.dex $PROJ/obj

echo "creating H file"
#javah -jni  -o HelloJNI.h obj/com/example/helloandroid/MainActivity.class
javah -classpath "$ANDROIDJAR:$PROJ/obj"  -o jni/HelloJNI.h com.example.helloandroid.MainActivity

#pakai ndk-build berhasil compile tapi gagal run
ndk-build

mv libs lib
#~/tools/android-ndk-r17/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-gcc \
#  --sysroot="${NDK}/platforms/android-19/arch-arm" \
#  -I /home/ok/tools/android-ndk-r17/sysroot/usr/include \
#      -march=armv7-a -mfpu=vfpv3-d16 -mfloat-abi=softfp -Wl,--fix-cortex-a8 \
#      -fPIC  -o libs/armeabi-v7a/libhello.so jni/HelloJNI.c

#      -fPIC -shared -o libs/armeabi-v7a/libhello.so jni/HelloJNI.c

echo "packaging apk"
$AAPT package -f -m -F $PROJ/bin/hello.unaligned.apk -M $PROJ/AndroidManifest.xml -S $PROJ/res -I $ANDROIDJAR



cp $PROJ/bin/classes.dex .
# cp libs/armeabi-v7a/libhello.so .

# ---- ./aapt add $PROJ/bin/hello.unaligned.apk classes.dex => mempengaruhi hasil dari apk( direktori di ikut sertakan)
# inti add : mengcompile atau menyatukan beberapa object
$AAPT             add                $PROJ/bin/hello.unaligned.apk classes.dex assets/index.html  \
  lib/armeabi-v7a/libhello.so #libs/x86/libhello.so libs/arm64-v8a/libhello.so libs/x86_64/libhello.so

$APKSIGNER sign --ks mykey.keystore --ks-pass pass:123456 ./bin/hello.unaligned.apk


adb install -r $PROJ/bin/hello.unaligned.apk 
exit 0 



 arm64-v8a/
 armeabi-v7a/
 x86/
 x86_64/






# https://stackoverflow.com/questions/17963508/how-to-install-android-sdk-build-tools-on-the-command-line
# ./tools/android update sdk --filter android-19 --use-sdk-wrapper

# ./android update sdk --filter tools,platform-tools,build-tools-19.0.1 --use-sdk-wrapper
# ./tools/android update sdk --filter build-tools-27.0.3 --use-sdk-wrapper

# https://www.java-tips.org/other-api-tips-100035/148-jni/1378-simple-example-of-using-the-java-native-interface.html
# https://www.hanshq.net/command-line-android.html

# http://blog.dornea.nu/2015/07/01/debugging-android-native-shared-libraries/
# https://www.arc4dia.com/blog/building-and-debugging-command-line-programs-on-android/

# E/AndroidRuntime( 3271): java.lang.UnsatisfiedLinkError: Couldn't load hello from loader dalvik.system.PathClassLoader[DexPathList[[zip file "/data/app/com.example.helloandroid-1.apk"],nativeLibraryDirectories=[/data/app-lib/com.example.helloandroid-1, /vendor/lib, /system/lib]]]: findLibrary returned null
# E/AndroidRuntime( 3271): 	at com.example.helloandroid.MainActivity.<clinit>(MainActivity.java:18)
# W/ActivityManager(  561):   Force finishing activity com.example.helloandroid/.MainActivity
# V/WindowManager(  561): Changing focus from null to Window{41a20830 u0 Application Error: com.example.helloandroid}
# I/BufferQueue(  189): [Application Error: com.example.helloandroid](this:0x42bbe008,api:0) setConsumerName: Application Error: com.example.helloandroid
# D/AES     (  561):     process : com.example.helloandroid


